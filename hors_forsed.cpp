#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <time.h>

//глобальные константы, определяющие размерность массивов
const int hor = 8, ver = 8, cnt = 1000;
//прототип функции для вывода массива, имитирующего шахматную доску, на экран 
void printBoard(int[][ver]); 

using namespace std;

int main() {
	//выделяем память для массива, эмитирующего шахматную доску
	/*int board[hor][ver] = {0}; 
	//количество доступных ходов из клетки
	int movesCount[hor][ver] = {
		{2,3,4,4,4,4,3,2}, 
		{3,4,6,6,6,6,4,3},
		{4,6,8,8,8,8,6,4},
		{4,6,8,8,8,8,6,4},
		{4,6,8,8,8,8,6,4},
		{4,6,8,8,8,8,6,4},
		{3,4,6,6,6,6,4,3},
		{2,3,4,4,4,4,3,2}
	};*/
	//массив для регистрации количества ходов
	//int travels[cnt] = {0};
		int travels = 0;
	//описываем этими двумя массивами варианты ходов конем на доске(их всего восемь от 0 до 7)
	int horizontal[hor] = { 2, 1,-1,-2,-2,-1, 1, 2}; 
	int vertical[ver] = {-1,-2,-2,-1, 1, 2, 2, 1};
	//переменные, запоминающие текущие координаты нахождения коня
	int currentRow, currentColumn, mainRow, mainColumn;
	//переменные для проверки возможности хода
	int nextRow, nextColumn;
	//переменные для уменьшения количества доступных ходов
	int Row, Col;
	//переменная, определяющая вариант хода конем(от 0 до 7) 
	int moveNumber;
	//счетчик ходов конем 
	int counter = 0; 
	//флаг, что ход сделан
	bool moveDone = 0;

	srand(time(NULL));

	//очистка экрана
	system("clear");

	//предложение ввода координаты нахождения коня по горизонтали
	cout << "Enter a horizontal coordinate(0 - 7): "; 
	//сохранение введенных данных в переменной
	cin >> mainRow;
	//ввод координаты по вертикали 
	cout << "Enter a vertical coordinate(0 - 7): "; 
	//сохранение данных ввода 
	cin >> mainColumn; 	

	//начинаем ходить
	//for (int i = 0; i < cnt; i++) {
	do {
		currentRow = mainRow;
		currentColumn = mainColumn;
		counter = 0;
		int board[hor][ver] = {0}; 
		int movesCount[hor][ver] = {
			{2,3,4,4,4,4,3,2}, 
			{3,4,6,6,6,6,4,3},
			{4,6,8,8,8,8,6,4},
			{4,6,8,8,8,8,6,4},
			{4,6,8,8,8,8,6,4},
			{4,6,8,8,8,8,6,4},
			{3,4,6,6,6,6,4,3},
			{2,3,4,4,4,4,3,2}
		};

		do {
			//записываем в клетку номер хода
			board[currentRow][currentColumn] = ++counter;
			moveDone = 0;	//сброс флага сделанного хода

			//уменьшаем количество доступных ходов из клеток вокруг текущей
			for (int k = 0; k < 8; k++)	{
				Row = currentRow + horizontal[k];
				Col = currentColumn + vertical[k];
				if (Row >= 0 && Row <= 7 && Col >= 0 && Col <= 7)
					movesCount[Row][Col]--;
			}

			do {
				//выбираем следующий ход
				moveNumber = rand() % 8;
				//cout << moveNumber << "  ";

				nextRow = currentRow + horizontal[moveNumber]; 
				nextColumn = currentColumn + vertical[moveNumber];

				//проверяем, не выходит ли он за границу доски
				if (nextRow >= 0 && nextRow <= 7 && nextColumn >= 0 && nextColumn <= 7) {
					//если не ходили на нее еще то делаем на нее ход
					if(board[nextRow][nextColumn] == 0) {
						currentRow = nextRow; 
						currentColumn = nextColumn;
						moveDone = 1;
					}
				}
			}
			while (moveDone == 0);
		}
		while (movesCount[currentRow][currentColumn] > 0);
		//travels[i] = counter;
		travels ++;
		cout << setw(4) << counter;
		if ((travels+1) % 20 == 0)
			cout << endl ;
	}
	while (counter < 64);
/*
	int maxMovesCount = 0; 
	for (int i = 0; i < cnt; i++) {
		cout << setw(4) << travels[i];
		if ((i+1) % 20 == 0)
			cout << endl ;

		if (travels[i] > maxMovesCount)
			maxMovesCount = travels[i];
	}
*/
//	cout << endl << "The longest travel consists of " << maxMovesCount << " moves." << endl;
	cout << endl << "Total nuber of travels is " << travels << endl;
	//вызов функции для печати массива, моделирующего шахматную доску
	
	//printBoard(board); 
	//cout << "Moves count: " << counter << endl << endl;

	return 0;
}

//вывод массива, печатающего шахматную доску, на экран
void printBoard(int array[][ver]) {
	cout << endl;
	for(int j = 0; j < ver; j++) {
		for(int i = 0; i < hor; i++) {
			cout << setw(4) << array[i][j];
		}

		cout << endl << endl;
	}
}