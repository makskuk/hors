#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <time.h>

//глобальные константы, определяющие размерность массивов board и travels
const int hor = 8, ver = 8, cnt = 1000;
//описываем этими двумя массивами варианты ходов конем на доске(их всего восемь от 0 до 7)
int horizontal[hor] = { 2, 1,-1,-2,-2,-1, 1, 2}; 
int vertical[ver] = {-1,-2,-2,-1, 1, 2, 2, 1};

//прототип функции для вывода массива, имитирующего шахматную доску, на экран 
void printBoard(int[][ver]); 
//прототипы функций для обхода доски
void heuristic(int currentRow, int currentColumn);
void forsed(int currentRow, int currentColumn);

using namespace std;

int main() {
	int Row, Column, travelMode;

	//предложение ввода координаты нахождения коня по горизонтали
	cout << "Enter a horizontal coordinate(0 - 7): "; 
	//сохранение введенных данных в переменной
	cin >> Row;
	//ввод координаты по вертикали 
	cout << "Enter a vertical coordinate(0 - 7): "; 
	cin >> Column; 

	//предложение выбрать способ обхода
	cout << "Select travel mode(1 - heuristic, 2 - by random moves): ";
	cin >> travelMode;

	switch (travelMode) {
		case 1 : heuristic(Row, Column);
				break;
		case 2 : forsed(Row, Column);
				break;
		default : { 
				cout << "Incorrect mode entered!" << endl;
				return 1;
			}
	}
	return 0;
}

//вывод массива, печатающего шахматную доску, на экран
void printBoard(int array[][ver]) {
	cout << endl;
	for(int j = 0; j < ver; j++) {
		for(int i = 0; i < hor; i++) {
			cout << setw(4) << array[i][j];
		}

		cout << endl << endl;
	}
}

void heuristic(int currentRow, int currentColumn) {
	//выделяем память для массива, эмитирующего шахматную доску
	int board[hor][ver] = {0}; 
	
	//массив доступности ходов конем 
	int accessibility[hor][ver] = {
		{2,3,4,4,4,4,3,2}, 
		{3,4,6,6,6,6,4,3},
		{4,6,8,8,8,8,6,4},
		{4,6,8,8,8,8,6,4},
		{4,6,8,8,8,8,6,4},
		{4,6,8,8,8,8,6,4},
		{3,4,6,6,6,6,4,3},
		{2,3,4,4,4,4,3,2}
	};
	//переменная, определяющая вариант хода конем(от 0 до 7) 
	int moveNumber;
	//счетчик ходов конем 
	int counter = 0;

	//переменные currentRow и currentColumn модифицируються для изменения доступностей, поэтому сохраняем их
	int mainRow = currentRow, mainColumn = currentColumn;
	//переменные для записи координат следующего хода конем 
	int Row, Column; 

	//начинаем поиск решения
	for(int i = 1; i <= 64; i++) {
		board[mainRow][mainColumn] = ++counter;

		//делаем равным максимально возможной доступности
		int minAccessibility = 8;
		//временные переменные для исследования субдоступностей 
		int minA = 8, minB = 8; 

		//уменьшение доступности на единицу клеток, расположенных в одном ходу от выбранной
		for(moveNumber = 0; moveNumber <= 7; moveNumber++) {
			//запоминаем положение коня на доске перед модификацией для изменения доступностей
			currentRow = mainRow; 
			currentColumn = mainColumn;

			//исследуем доступность всех возможных ходов не выходящих за границы доски
			currentRow += horizontal[moveNumber]; 
			currentColumn += vertical[moveNumber];

			//не выходит ли за границы доски
			if(currentRow >= 0 && currentRow <= 7 && currentColumn >= 0 && currentColumn <= 7) {
				//уменьшаем доступность всех клеток в одном ходу
				accessibility[currentRow][currentColumn]--; 

				//если доступность больше, то меняем на меньшую
				if(minAccessibility > accessibility[currentRow][currentColumn] && board[currentRow][currentColumn] == 0) {
					//нашли минимальную доступность и ее координаты
					minAccessibility = accessibility[currentRow][currentColumn]; 

					//если не ходили на нее еще то делаем на нее ход
					if(board[currentRow][currentColumn] == 0) {
						//подготовили координаты к ходу на эту клетку
						Row = currentRow; 
						Column = currentColumn;
					}
					//временных переменные для нахождения минимальной доступности у тех, что меньше
					int RowA = currentRow, ColumnA = currentColumn; 

					for(int moveA = 0; moveA <= 7; moveA++) {
						RowA += horizontal[moveA];
						ColumnA += vertical[moveA];

						if(RowA >= 0 && RowA <= 7 && ColumnA >= 0 && ColumnA <= 7) {
							if(minA >= accessibility[RowA][ColumnA] && board[RowA][ColumnA] == 0)
								//наименьшая доступность следующего хода
								minA = accessibility[RowA][ColumnA]; 
						}
					}
				}

				//если доступности равны
				if(minAccessibility == accessibility[currentRow][currentColumn] && board[currentRow][currentColumn] == 0) {
					minAccessibility = accessibility[currentRow][currentColumn];

					//временных переменные для нахождения минимальной доступности у тех, что равны
					int RowB =currentRow, ColumnB = currentColumn; 

					for(int moveB = 0; moveB <= 7; moveB++) {
						RowB += horizontal[moveB];
						ColumnB += vertical[moveB];

						if(RowB >= 0 && RowB <= 7 && ColumnB >= 0 && ColumnB <= 7) {
							if(minB >= accessibility[RowB][ColumnB] && board[RowB][ColumnB] == 0)
								//наименьшая доступность следующего хода
								minB = accessibility[RowB][ColumnB]; 
						}
					}

					//если не ходили на нее еще то делаем на нее ход
					if(board[currentRow][currentColumn] == 0 && minB < minA) {
						//изменяем подготовленные для следующего хода координаты в случае, если доступность следующего хода меньше
						Row = currentRow; 
						Column = currentColumn;
					}
				}
			}
		}

		mainRow = Row;
		mainColumn = Column;
	}

	//вызов функции для печати массива, моделирующего шахматную доску
	printBoard(board); 
}

void forsed(int currentRow, int currentColumn) {
	//переменные, запоминающие текущие координаты нахождения коня
	int mainRow, mainColumn;
	//переменные для проверки возможности хода
	int nextRow, nextColumn;
	//переменные для уменьшения количества доступных ходов
	int Row, Col;
	//переменная, определяющая вариант хода конем(от 0 до 7) 
	int moveNumber;
	//счетчик ходов конем 
	int counter = 0; 
	//флаг, что ход сделан
	bool moveDone = 0;
	//массив для регистрации количества ходов
	int travels[cnt] = {0};

	srand(time(NULL));
	mainRow = currentRow;
	mainColumn = currentColumn;	

	//начинаем ходить
	for (int i = 0; i < cnt; i++) {
		currentRow = mainRow;
		currentColumn = mainColumn;
		counter = 0;
		int board[hor][ver] = {0}; 
		int movesCount[hor][ver] = {
			{2,3,4,4,4,4,3,2}, 
			{3,4,6,6,6,6,4,3},
			{4,6,8,8,8,8,6,4},
			{4,6,8,8,8,8,6,4},
			{4,6,8,8,8,8,6,4},
			{4,6,8,8,8,8,6,4},
			{3,4,6,6,6,6,4,3},
			{2,3,4,4,4,4,3,2}
		};

		do {
			//записываем в клетку номер хода
			board[currentRow][currentColumn] = ++counter;
			moveDone = 0;	//сброс флага сделанного хода

			//уменьшаем количество доступных ходов из клеток вокруг текущей
			for (int k = 0; k < 8; k++)	{
				Row = currentRow + horizontal[k];
				Col = currentColumn + vertical[k];
				if (Row >= 0 && Row <= 7 && Col >= 0 && Col <= 7)
					movesCount[Row][Col]--;
			}

			do {
				//выбираем следующий ход
				moveNumber = rand() % 8;
				//cout << moveNumber << "  ";

				nextRow = currentRow + horizontal[moveNumber]; 
				nextColumn = currentColumn + vertical[moveNumber];

				//проверяем, не выходит ли он за границу доски
				if (nextRow >= 0 && nextRow <= 7 && nextColumn >= 0 && nextColumn <= 7) {
					//если не ходили на нее еще то делаем на нее ход
					if(board[nextRow][nextColumn] == 0) {
						currentRow = nextRow; 
						currentColumn = nextColumn;
						moveDone = 1;
					}
				}
			}
			while (moveDone == 0);
		}
		while (movesCount[currentRow][currentColumn] > 0);
		travels[i] = counter;
	}

	// выводим таблицу путешествий, находим самое длинное
	int maxMovesCount = 0; 
	for (int i = 0; i < cnt; i++) {
		cout << setw(4) << travels[i];
		if ((i+1) % 20 == 0)
			cout << endl ;

		if (travels[i] > maxMovesCount)
			maxMovesCount = travels[i];
	}
	cout << endl << "The longest travel consists of " << maxMovesCount << " moves." << endl;
	
	//вызов функции для печати массива, моделирующего шахматную доску
	//printBoard(board); 
	//cout << "Moves count: " << counter << endl << endl;
}
